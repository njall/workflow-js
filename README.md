# About

Implementation Study: [Registry Integration from a User Perspective](https://www.elixir-europe.org/about-us/implementation-studies/elixir-integration-user-perspective).

The aim of the project is to bring tools, training and other ELIXIR Registry resources together through Concept Maps. 

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
