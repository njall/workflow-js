export function hashCode(str) { // java String#hashCode
  let hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}

export function isEmpty(obj) {
  return Object.keys(obj).length === 0;
}

export function intToRGB(i) {
  let c = (i & 0x00FFFFFF)
    .toString(16)
    .toUpperCase();

  return "00000".substring(0, 6 - c.length) + c;
}

export function encodeQueryString(params) {
  const keys = Object.keys(params);
  return keys.length
    ? "?" + keys
    .map(key => encodeURIComponent(key)
      + "=" + encodeURIComponent(params[key]))
    .join("&")
    : "";
}

const PALETTE = {
  "Quantification":               "#6ce9cc",
  "Service management":           "#6be7af",
  "Correlation":                  "#80e493",
  "Calculation":                  "#9edf7c",
  "Conversion":                   "#b9d96b",
  "Clustering":                   "#d3d360",
  "Generation":                   "#eaca5d",
  "Classification":               "#f5bf61",
  "Analysis":                     "#f4b46c",
  "Design":                       "#f2a87d",
  "Mapping":                      "#f19f95",
  "Validation":                   "#f19aaf",
  "Modelling and simulation":     "#f099cd",
  "Optimisation and refinement":  "#f19feb",
  "Comparison":                   "#f2a8fa",
  "Prediction and recognition":   "#f0b4fa",
  "Data handling":                "#ccbefa",
  "Visualisation":                "#9fc9fa",
  "Indexing":                     "#67d2fa",
  "Annotation":                   "#63dafb",
  "Alignment":                    "#8595e1",
};

export function termToColor(term) {
  return PALETTE[term.subroot[2]];
}
