import './styles/styles.scss';

import Split from 'split.js';
import cytoscape from 'cytoscape';
import panzoom from 'cytoscape-panzoom';
import cxtmenu from 'cytoscape-cxtmenu';
import dagre from 'cytoscape-dagre';

import nodeHtmlLabel from 'cytoscape-node-html-label';

import classnames from 'classnames';
import MarkdownIt from 'markdown-it';

import Choices from 'choices.js';

import edgehandles from 'cytoscape-edgehandles';

import FileSaver from 'file-saver';


import {isEmpty, encodeQueryString, termToColor} from './scripts/utils.js';

import EdamSelect from "./scripts/vendor/edam-select";

const DEBUG = true;

const BIOTOOLS_API = "https://bio.tools/api/tool";
const TESS_API = "https://tess.elixir-europe.org/materials.json";

const FAIRSHARING_API = "https://fairsharing.org/api/all/summary/";
const FAIRSHARING_API_KEY = '7f1af03ac7aec02b572656550f37d2f1e8f77b7b'

const FAIRSHARING_RESOURCE_TEMPLATE = "%base%/api/database/summary/%id%"

import templates from './scripts/templates';

// register cytoscape extensions
panzoom(cytoscape);
edgehandles(cytoscape);
cxtmenu(cytoscape);
dagre(cytoscape);
nodeHtmlLabel(cytoscape);

const ACTIONS_WITH_DOCS = ['init', 'operation', 'data', 'assoc_resources']

class Sidebar {
  constructor() {
    this.el = null;
    this.split = null;
    this.expanded = false;
  }

  render(sidebar, variables) {
    //Setup the HTML for the sidebar
    // if no docs for this action, or in view mode - just render sidebar
    // else render sidebar + docs
    if (  !(variables.template &&
            Object.keys(variables.template)[0] &&
            ACTIONS_WITH_DOCS.includes(Object.keys(variables.template)[0])) ||
          (variables.mode.view)
        ) {
      this.el = templates.sidebar(variables);
      sidebar.innerHTML = this.el;
      // Register collaspible description extension code
      this.toggleDescription(sidebar)

    } else {
      this.el = templates.sidebar(variables).concat(templates.docs(variables));
      sidebar.innerHTML = this.el;
      // Register the the sidebar and docs with Split
      this.split = Split(['.workflow-diagram-sidebar-content', '.workflow-help-section'], {
            direction: 'vertical',
            sizes: [81,19],
            cursor: 'row-resize'
      });
      // Extend docs if workflow-help-title is clicked
      sidebar.querySelector('.workflow-help-title').addEventListener('click', (e) => {
        this.toggleHelp()
      });
      // Register nav items as clickable links
      this.registerHelpLinks(sidebar)
    }
    
    return this
  }


  registerHelpLinks(sidebar){
    sidebar.querySelectorAll('.nav-item').forEach((a) => {
      a.addEventListener('click', (e)=>{
        e.preventDefault();
        return false
      })
    })
  }

  toggleHelp(){
    var expanded = this.expanded
    if (expanded){
        this.split.setSizes([81,19]);
        document.querySelectorAll('.chevron')
        .forEach((a) =>
          a.classList.replace('fa-chevron-circle-down', 'fa-chevron-circle-up')
        )
        document.querySelector('.workflow-help-section').style.overflow = 'hidden'
    } else {
      this.split.setSizes([19, 81]);
      document.querySelectorAll('.chevron')
        .forEach((a) =>
          a.classList.replace('fa-chevron-circle-up', 'fa-chevron-circle-down')
        )
        document.querySelector('.workflow-help-section').style.overflow = 'scroll'
    }
    this.expanded = !expanded
  }

  toggleDescription(sidebar){
    sidebar.querySelectorAll('.more').forEach((a) => {
      a.addEventListener('click', (e)=>{
        let description = e.target.nextElementSibling
        if (description.style.display === 'block'){
          description.style.display = 'none';
          e.target.innerText = 'more about this ' + e.target.dataset['type']
        } else {
          description.style.display = 'block';
          e.target.innerText = 'less about this ' + e.target.dataset['type']
        }
        e.preventDefault();
        return false
      })
    })
    return this
  }

}

class History {
  constructor(wf) {
    this.wf = wf;
    this.stack = [];
    this.index = 0;
    this.cy = wf.cy;
  }

  modify(type) {
    this.stack = this.stack.slice(0, this.index);
    this.index += 1;
    this.stack.push({
      type,
      elements: this.cy.elements('node.wrap, node.data, node.operation, edge').clone()
    });
  }

  undo() {
    if (this.index > 1) {
      this.index -= 1;
      this.restore();
    }
  }

  redo() {
    if (this.index < this.stack.length) {
      this.index += 1;
      this.restore();
    }
  }

  restore() {
    this.cy.elements('node.wrap, node.data, node.operation').removeListener('free');
    this.cy.elements('node.wrap, node.data, node.operation').removeListener('unselect');
    this.cy.elements().remove();

    if (this.index - 1 >= 0) {
      let nodes = this.stack[this.index - 1].elements.clone().restore();
      this.cy.$(':selected').unselect();

      this.cy.elements('node.wrap, node.data, node.operation').on('free', () => {
        this.modify(Workflow.ACTION.POSITION_CHANGE);
      });
      this.cy.elements('node.wrap, node.data, node.operation').on('unselect', () => {
        if (wf.dataHasChanged) {
          wf.dataHasChanged = false;
          this.modify(Workflow.ACTION.DATA_CHANGE);
        }
      });
    }
  }

}

class Workflow {
  constructor(sel, params) {
    this.id = Workflow.incrementId();

    this.nodeCounter = 0;

    this.history = null;

    this.converter = params.converter;

    this.counter = {
      get: () => `node-${this.nodeCounter}`,
      inc: () => `node-${++this.nodeCounter}`,
    };


    if (typeof sel === 'string') {
      this.el = document.querySelector(sel);
    } else {
      this.el = sel;
    }
    this.el.id = `workflow-${this.id}`;
    this.el.className = 'workflow';

    this.cy = null; // cytoscape instance
    this.split = null; // split instance

    this.md = new MarkdownIt();

    if (params.title.length > 0) {
      this.title = params.title;
    }
    else {
      this.title = "Untitled workflow";
    }

    if (params.editable) {
      this.editable = true;
      this.mode = Workflow.MODE.EDIT;
    } else {
      this.editable = false;
      this.mode = Workflow.MODE.VIEW;
    }

    this.sidebar = new Sidebar();

    this.empty = true;

    this.dataHasChanged = false;

    this.init(params.preload);
  }

  destroy() {
    this.split.destroy();
  }

  init(preload) {
    this.el.innerHTML = templates.layout();

    this.split = Split(['.workflow-diagram-content', '.workflow-diagram-sidebar'], {
      sizes: [75, 25],
      minSize: 400,
      onDrag: () => {
        this.cy.resize();
      }
    });

    let data = {
      layout: {
        name: 'preset'
      },
      style: [
        {
          selector: '.position',
          css: {
            width: '25px',
            height: '25px'
          }
        },
        {
          selector: '.operation',
          css: {
            'shape': 'ellipse',
            'content': 'data(name)',
            // 'background-opacity': 0.5,
            'text-valign': 'center',
            'text-halign': 'center',
            'width': '150px',
            'height': 'label',
            'font-size': '12px',
            'border-width': '2px',
            'border-color': '#000',
            'border-opacity': 0.1,
            'text-wrap': 'wrap',
            'text-max-width': '130px',
            'padding-left': 5 + 'px',
            'padding-right': 5 + 'px',
            'padding-top': '12px',
            'padding-bottom': '12px',
            'background-color': 'data(color)',
            'color': '#000',
          }
        },
        {
          selector: '.data',
          css: {
            'background-color': 'white',
            'shape': 'roundrectangle',
            'content': 'data(name)',
            'text-valign': 'center',
            'text-halign': 'center',
            'width': '150px',
            'height': 'label',
            'font-size': '12px',
            'border-width': '2px',
            'border-color': '#000',
            'border-opacity': 0.1,
            'text-wrap': 'wrap',
            'text-max-width': '130px',
            'padding-left': 5 + 'px',
            'padding-right': 5 + 'px',
            'padding-top': '10px',
            'padding-bottom': '10px',
            'background-color': 'lightgrey',
            'color': '#000',
          }
        },
        {
          selector: '.input',
          css: {
            'background-color': 'white',
            'shape': 'roundrectangle',
            'content': 'data(name)',
            'background-opacity': 0.5,
            'text-valign': 'center',
            'text-halign': 'center',
            'width': 'label',
            'height': '30px',
            'font-size': '9px',
            'border-width': '2px',
            'border-color': '#000',
            'border-opacity': 0.1,
            'text-wrap': 'wrap',
            'text-max-width': '130px',
            'padding-left': 5 + 'px',
            'padding-right': 5 + 'px',
          }
        },
        {
          selector: '.output',
          css: {
            'background-color': 'white',
            'shape': 'roundrectangle',
            'content': 'data(name)',
            'background-opacity': 0.5,
            'text-valign': 'center',
            'text-halign': 'center',
            'width': 'label',
            'height': '30px',
            'font-size': '9px',
            'border-width': '2px',
            'border-color': '#000',
            'border-opacity': 0.1,
            'text-wrap': 'wrap',
            'text-max-width': '130px',
            'padding-left': 5 + 'px',
            'padding-right': 5 + 'px',
          }
        },
        {
          selector: '.wrap',
          css: {
            'content': 'data(name)',
            'shape': 'roundrectangle',
            'font-size': '12px',
            'color': 'grey',
          }
        },
        {
          selector: ':selected',
          css: {
            'border-width': '2px',
            'border-color': '#2A62E4',
            'border-opacity': 1,
          }
        },
        {
          selector: '.eh-handle',
          css: {
            'background-color': '#007eff',
            'width': '5px',
            'height': '5px'
          }
        },

        {
          selector: 'edge',
          style: {
            'curve-style': 'bezier',
            'target-arrow-shape': 'triangle',
            'line-style': 'dashed'
          }
        },

        {
          selector: 'edge.required',
          style: {
            'line-style': 'solid'
          }
        },

        // some style for the extension
        {
          selector: '.eh-handle',
          style: {
            'background-color': '#007eff',
            'width': 12,
            'height': 12,
            'shape': 'ellipse',
            'overlay-opacity': 0,
            'border-width': 12, // makes the handle easier to hit
            'border-opacity': 0
          }
        },

        {
          selector: '.eh-hover',
          style: {
            'background-color': '#007eff',
          }
        },
        {
          selector: '.eh-source',
          style: {
            'border-width': 2,
            'border-color': '#007eff',
            'border-opacity': 1,
          }
        },

        {
          selector: '.eh-target',
          style: {
            'border-width': 2,
            'border-color': 'green',
            'border-opacity': 1,
          }
        },
        {
          selector: '.wrong-connection',
          style: {
            // 'background-color': 'green',
            'border-color': 'red',
          }
        },

        {
          selector: '.input.eh-preview, .eh-ghost-edge',
          style: {
            'border-opacity': 1,
            'line-color': '#007eff',
            'target-arrow-color': '#007eff',
            'source-arrow-color': '#007eff'
          }
        },
        {
          selector: '.eh-ghost-edge.eh-preview-active',
          style: {
            'opacity': 0
          }
        }
      ],
    };

    if (preload && !isEmpty(preload)) {
      data['elements'] = preload;
      if (data.elements.nodes) {
        this.nodeCounter += data.elements.nodes.length;
      }
    }


    // Set maxZoom and minZoom.
    this.cy = cytoscape(Object.assign(data, {
      container: this.el.querySelector('.cytoscape'),
      maxZoom: 3,
      minZoom: 0.5,
    }));

    // Method used to add html labels to each node.
    this.cy.nodeHtmlLabel([
      {
        query: '.data',
        halign: 'right', // title vertical position. Can be 'left',''center, 'right'
        valign: 'center', // title vertical position. Can be 'top',''center, 'bottom'
        halignBox: 'right', // title vertical position. Can be 'left',''center, 'right'
        valignBox: 'center', // title relative box vertical position. Can be 'top',''center, 'bottom'
        cssClass: 'dataHtmlLabel', // any classes will be as attribute of <div> container for every title
        tpl: function(data) {
          return '<p class="cy_data_format">' + data.dataFormat + '</p>';
        }
      },
      {
        query: '.operation',
        halign: 'right', // title vertical position. Can be 'left',''center, 'right'
        valign: 'center', // title vertical position. Can be 'top',''center, 'bottom'
        halignBox: 'right', // title vertical position. Can be 'left',''center, 'right'
        valignBox: 'center', // title relative box vertical position. Can be 'top',''center, 'bottom'
        cssClass: 'operationHtmlLabel', // any classes will be as attribute of <div> container for every title
        tpl: function(data) {
          return '<p class="cy_operation_tool">' + data.operationTool + '</p>';
        }
      }
    ]);

    this.cy.reset();

    this.cy.panzoom();

    let defaults = {
      menuRadius: 100,
      selector: 'core',
      commands: [
        {
          fillColor: 'rgba(200, 200, 200, 0.25)',
          content: 'Add<br>Operation<br>node',
          contentStyle: {},
          select: (ele, event) => {
            this.state = 'adding operation node';
            this.handleClick(event);
          },
          enabled: true
        },
        {
          fillColor: 'rgba(200, 200, 200, 0.25)',
          content: 'Add<br>Data<br>node',
          contentStyle: {},
          select: (ele, event) => {
            this.state = 'adding data node';
            this.handleClick(event);
          },
          enabled: true
        },
      ],
      activeFillColor: '#e0eafb',
      activePadding: 0,
      indicatorSize: 24,
      separatorWidth: 3,
      spotlightPadding: 10,
      minSpotlightRadius: 24,
      maxSpotlightRadius: 38,
      openMenuEvents: 'cxttapstart taphold',
      itemColor: 'black',
      itemTextShadowColor: 'transparent',
      zIndex: 9999,
      atMouse: true
    };

    let menu = this.cy.cxtmenu(defaults);

    let nodeMenu = this.cy.cxtmenu({
      menuRadius: 100,
      selector: 'node',
      commands: [
        {
          fillColor: '#f6f6f6',
          content: 'Add next node',
          contentStyle: {},
          select: (ele, event) => {
            this.addNextNode(ele.id(), ele.data().type, ele.data().required);
          },
          enabled: true
        },
        {
          fillColor: '#f6f6f6',
          content: 'Select branch',
          contentStyle: {},
          select: (ele, event) => {
            this.selectBranch(ele.id(), ele.data().type);
          },
          enabled: true
        },
        {
          fillColor: '#fff0f0',
          content: 'Remove node',
          contentStyle: {},
          select: (ele, event) => {
            this.removeNode(ele.id(), ele.data().type);
          },
          enabled: true
        },
      ],
      activeFillColor: '#e0eafb',
      activePadding: 0,
      indicatorSize: 24,
      separatorWidth: 3,
      spotlightPadding: 10,
      minSpotlightRadius: 24,
      maxSpotlightRadius: 38,
      openMenuEvents: 'cxttapstart taphold',
      itemColor: 'black',
      itemTextShadowColor: 'transparent',
      zIndex: 9999,
      atMouse: true
    });

    this.el.querySelector('.workflow-diagram-sidebar').style.height = this.el.querySelector('.workflow-diagram-sidebar').clientHeight + 'px';

    // Handle select event.
    this.cy.on('select', (e) => {
      this.populate(e);

    });

    this.cy.on('unselect', (e) => {
      this.populate(e);
    });

    this.cy.on('tap', (e) => {
      this.handleClick(e);
    });

    this.cy.on('tap', (e) => {
      this.handleClick(e);
    });


    // Method for clicking on one specific node.
    this.focusControl = (e) => {
      if (e.target.parentNode && e.target.parentNode.parentNode &&
        e.target.parentNode.parentNode.parentNode &&
        e.target.parentNode.parentNode.parentNode === this.el.querySelector('.workflow-diagram-content')) {
        this.focus = true;
      } else {
        this.focus = false;
      }
    };
    document.addEventListener('click', this.focusControl);

    this.keyboardControlWrap = (e) => {
      this._keyboardControl(e)
    };
    document.addEventListener('keydown', this.keyboardControlWrap);

    if (this.mode === Workflow.MODE.EDIT) {
      this.initEdgeHandlers();

      this.edamControl = (e) => {
        this._edamControl(e)
      };
      document.addEventListener('edam:change', this.edamControl);
    }


    // toolbar
    this.toolbar = this.el.querySelector('.workflow-toolbar');
    if (!this.editable) {
      this.toolbar.style.display = "none";
    }

    if (this.converter) {
      this._initConverter(this.converter);
    }

    this.toolbar.querySelector('.export').addEventListener('click', () => {

      this.cy.elements('.eh-handle').remove();
      let json = JSON.stringify(this.export());
      let blob = new Blob([json], {type: "application/json"});

      FileSaver.saveAs(blob, `${this.title}.json`);
    });

    this.toolbar.querySelector('.import').addEventListener('click', () => {
      this.toolbar.querySelector('.file').click();
    });

    this.toolbar.querySelector('.auto-layout').addEventListener('click', () => {
      this.cy.layout({ name: 'dagre'}).run();
      this.history.modify(Workflow.ACTION.LAYOUT);
    });

    this.toolbar.querySelector('.file').addEventListener('change', (e) => {
      let files = e.target.files;
      let file = files[0];
      let reader = new FileReader();

      this.title = files.item(0).name.split('.')[0];

      reader.onload = (event) => {
        let data = JSON.parse(event.target.result);

        this.import(data);

      };
      reader.readAsText(file)
    });


    // history
    this.history = new History(this);

    this.cy.$('node').on('free', () => {
      this.history.modify(Workflow.ACTION.POSITION_CHANGE);
    });
    this.cy.$('node').on('unselect', () => {
      if (this.dataHasChanged) {
        this.dataHasChanged = false;
        this.history.modify(Workflow.ACTION.DATA_CHANGE);
      }
    });

    this.toolbar.querySelector('.redo').addEventListener('click', () => {
      this.history.redo();
    });
    this.toolbar.querySelector('.undo').addEventListener('click', () => {
      this.history.undo();
    });

    // mode
    this.toolbar.querySelector('.view').addEventListener('click', () => {
      this.mode = Workflow.MODE.VIEW;
      this.destroy();
      this.init(this.export());
      this.toolbar.querySelector('.view').style.color = '#525252';
    });

    this.toolbar.querySelector('.edit').addEventListener('click', () => {
      this.mode = Workflow.MODE.EDIT;
      this.destroy();
      this.init(this.export());
      this.toolbar.querySelector('.edit').style.color = '#525252';
    });


    // first screen
    this.history.modify(Workflow.ACTION.INIT);
    this.renderAddNode(true);
  }

  setState(state) {
    this.state = state;
    switch (state) {
      case 'adding operation node':
        this.focusCanvas();
        break;
      case 'adding data node':
        this.focusCanvas();
        break;
      default:
        this.blurCanvas();
    }
  }

  _initConverter(converter) {
    let converterWrap = document.createElement('div');
    converterWrap.innerHTML = templates.converter();

    this.el.appendChild(converterWrap);

    converterWrap.querySelector('.alert').style.display = 'none';

    converterWrap.querySelector('.convert').addEventListener('click', () => {
      let data = converterWrap.querySelector('.data-to-convert').value;
      let format = converterWrap.querySelector('input[name="format"]:checked').value;

      let http = new XMLHttpRequest();
      let url = converter;
      let params = 'data=' + data + '&format=' + format;
      http.open('POST', url, true);

      http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

      http.onreadystatechange = () => {
        if(http.readyState === 4 && http.status === 200) {
          console.log(http.responseText);

          converterWrap.querySelector('.convert-alert').innerHTML = '';
          converterWrap.querySelector('.convert-alert').style.display = 'none';

          this.import(JSON.parse(http.responseText));

        } else {

          converterWrap.querySelector('.convert-alert').innerHTML = 'Something went wrong, please, contact administrators to check the console and server logs.';
          converterWrap.querySelector('.convert-alert').style.display = 'block';

          console.log(http);
        }
      };
      http.send(params);
    });
  }

  _keyboardControl(e) {
    // if (e.keyCode === 13) { // fix for return key
    //   e.preventDefault();
    //   return false;
    // }

    if (this.focus) {
      let selectedNodes = this.cy.$(':selected');
      if (selectedNodes.length === 1) {
        if (e.keyCode === 40) {        // down
          e.preventDefault();
          let nextNodes = this.cy.$(':selected').outgoers('node');
          if (nextNodes.length > 0) {

            let leftmostNode = null;
            let leftmostPosition = Infinity;

            for (let i = 0; i < nextNodes.length; i += 1) {
              if (nextNodes[i].position().x < leftmostPosition) {
                leftmostPosition = nextNodes[i].position().x;
                leftmostNode = nextNodes[i];
              }
            }

            this.cy.$(':selected').unselect();
            leftmostNode.select();

            this.cy.animate({center: {eles: this.cy.$(':selected')}, duration: 300});
          }
        } else if (e.keyCode === 38) { // up
          e.preventDefault();
          let prevNodes = this.cy.$(':selected').incomers('node');
          if (prevNodes.length > 0) {

            let leftmostNode = null;
            let leftmostPosition = Infinity;

            for (let i = 0; i < prevNodes.length; i += 1) {
              if (prevNodes[i].position().x < leftmostPosition) {
                leftmostPosition = prevNodes[i].position().x;
                leftmostNode = prevNodes[i];
              }
            }

            this.cy.$(':selected').unselect();
            leftmostNode.select();

            this.cy.animate({center: {eles: this.cy.$(':selected')}, duration: 300});
          }
        } else if (e.keyCode === 39) { // right
          e.preventDefault();
          let prevNodes = this.cy.$(':selected').incomers('node');
          if (prevNodes.length > 0) {
            let siblingNodes = prevNodes.outgoers('node');

            let currentPosition = this.cy.$(':selected').position().x;
            let minToRightPosition = Infinity;
            let closestToRightNode = null;

            for (let i = 0; i < siblingNodes.length; i += 1) {
              if (siblingNodes[i].id() !== this.cy.$(':selected').id()) {
                let position = siblingNodes[i].position().x;
                if (position > currentPosition) {
                  if (position < minToRightPosition) {
                    minToRightPosition = position;
                    closestToRightNode = siblingNodes[i];
                  }
                }
              }
            }

            if (closestToRightNode) {
              this.cy.$(':selected').unselect();
              closestToRightNode.select();
              this.cy.animate({center: {eles: this.cy.$(':selected')}, duration: 300});
            }
          }
        } else if (e.keyCode === 37) { // left
          e.preventDefault();
          let prevNodes = this.cy.$(':selected').incomers('node');
          if (prevNodes.length > 0) {
            let siblingNodes = prevNodes.outgoers('node');

            let currentPosition = this.cy.$(':selected').position().x;
            let minToLeftPosition = -Infinity;
            let closestToLeftNode = null;

            for (let i = 0; i < siblingNodes.length; i += 1) {
              if (siblingNodes[i].id() !== this.cy.$(':selected').id()) {
                let position = siblingNodes[i].position().x;
                if (position < currentPosition) {
                  if (position > minToLeftPosition) {
                    minToLeftPosition = position;
                    closestToLeftNode = siblingNodes[i];
                  }
                }
              }
            }

            if (closestToLeftNode) {
              this.cy.$(':selected').unselect();
              closestToLeftNode.select();
              this.cy.animate({center: {eles: this.cy.$(':selected')}, duration: 300});
            }
          }
        }
      }
    }
  }

  _edamControl(e) {

    if (e) {
      this.cy.$(':selected').data().custom = '';
      if (!e.detail.preselected) {
        if (e.detail.type === 'operation') {
          this.dataHasChanged = true;
          if (e.detail.selected.length > 0) {
            this.cy.$(':selected').data().operation = e.detail.selected.map(d => d[0]);
            this.cy.$(':selected').data().color = termToColor(e.detail.selected[0]); // intToRGB(hashCode(e.detail.selected[0][2]));
            this.cy.$(':selected').css('background-color', this.cy.$(':selected').data().color);

            // this.cy.$(':selected').data().name = this.cy.$(':selected').data().name.split('\n')[0];
            this.cy.$(':selected').data().name = this.cy.$(':selected').data().operation.map(d => EdamSelect.getTermById('operation', d)[2]).join('\n');
            this.cy.$(':selected').data().names = this.cy.$(':selected').data().name.split('\n')
            // this.cy.$(':selected').data().name = EdamSelect.getTermById('operation', e.detail.selected[0][0])[2];

            this.cy.$(':selected').removeClass('operation').addClass('operation'); // force redraw
            document.querySelector('.workflow-diagram-sidebar-title').style.backgroundColor = this.cy.$(':selected').data().color;
          } else {
            this.cy.$(':selected').data().operation = null;
            this.cy.$(':selected').data().color = null;
            this.cy.$(':selected').css('background-color', null);

            this.cy.$(':selected').data().name = "Operation";
            this.cy.$(':selected').removeClass('operation').addClass('operation'); // force redraw

            document.querySelector('.workflow-diagram-sidebar-title').style.backgroundColor = null;
          }
        } else if (e.detail.type === 'data') {
          this.dataHasChanged = true;
          if (e.detail.selected.length > 0) {
            this.cy.$(':selected').data().data = e.detail.selected[0][0];

            this.cy.$(':selected').data().name = EdamSelect.getTermById('data', e.detail.selected[0][0])[2];

            if (this.cy.$(':selected').data().formats) {
              this.cy.$(':selected').data().name = this.cy.$(':selected').data().name.split('\n')[0];
              // this.cy.$(':selected').data().name += ('\n[' + this.cy.$(':selected').data().formats.map(d => EdamSelect.getTermById('format', d)[2]).join(', ') + ']');
            }

            this.cy.$(':selected').removeClass('data').addClass('data'); // force redraw
          } else {
            this.cy.$(':selected').data().data = null;

            this.cy.$(':selected').data().name = "Data";
            this.cy.$(':selected').removeClass('data').addClass('data'); // force redraw
          }
        } else if (e.detail.type === 'format') {
          this.dataHasChanged = true;
          if (e.detail.selected.length > 0) {
            this.cy.$(':selected').data().formats = e.detail.selected.map(d => d[0]);
            this.cy.$(':selected').data().format_terms = this.cy.$(':selected').data().formats.map(d => EdamSelect.getTermById('format', d));
            // this.cy.$(':selected').data().name = this.cy.$(':selected').data().name.split('\n')[0];
            // this.cy.$(':selected').data().name += ('\n[' + this.cy.$(':selected').data().formats.map(d => EdamSelect.getTermById('format', d)[2]).join(', ') + ']');
            var updatedNode = this.cy.$(':selected');
            var edges  = updatedNode.connectedEdges();
            updatedNode.data().dataFormat = 'Format(s): ' + ('\n[' + this.cy.$(':selected').data().formats.map(d => EdamSelect.getTermById('format', d)[2]).join(', ') + ']');
            this.cy.remove(this.cy.$(':selected'));
            // Add the updated nodes.
            this.cy.add(updatedNode);
            // Add connected edges.
            for (var i = 0;i < edges.length;i++) {
              this.cy.add(edges[i]);
            }
            this.cy.$(':selected').removeClass('data').addClass('data'); // force redraw
          } else {
            this.cy.$(':selected').data().formats = null;
            var updatedNode = this.cy.$(':selected');
            var edges  = updatedNode.connectedEdges();
            updatedNode.data().dataFormat = '';
            this.cy.remove(this.cy.$(':selected'));
            this.cy.add(updatedNode);
            // Add connected edges.
            for (var i = 0;i < edges.length;i++) {
              this.cy.add(edges[i]);
            }
            this.cy.$(':selected').removeClass('data').addClass('data'); // force redraw
          }
        }
      }
    } else {

      this.cy.$(':selected').data().data = null;
      this.cy.$(':selected').data().operation = null;
      this.cy.$(':selected').data().color = "lightgrey";
      this.cy.$(':selected').css('background-color', null);

      if (this.cy.$(':selected').data().custom) {
        this.cy.$(':selected').data().name = this.cy.$(':selected').data().custom;

        if (this.cy.$(':selected').data().type === 'operation') {
          this.cy.$(':selected').removeClass('operation').addClass('operation'); // force redraw
        } else if (this.cy.$(':selected').data().type === 'data') {
          this.cy.$(':selected').removeClass('data').addClass('data'); // force redraw
        }

      } else {
        if (this.cy.$(':selected').data().type === 'operation') {
          this.cy.$(':selected').data().name = 'Operation';
          this.cy.$(':selected').removeClass('operation').addClass('operation'); // force redraw
        } else if (this.cy.$(':selected').data().type === 'data') {
          this.cy.$(':selected').data().name = 'Data';
          this.cy.$(':selected').removeClass('data').addClass('data'); // force redraw
        }
      }
    }
  }


  focusCanvas() {
    this.el.querySelector('.workflow-diagram-content').classList.add('focus');
  }

  blurCanvas() {
    this.el.querySelector('.workflow-diagram-content').classList.remove('focus');
  }

  handleClick(e) {
    if (this.state === 'adding operation node') {
      this.placeNode(e.position, 'operation');
      this.history.modify(Workflow.ACTION.ADD_NODE);
    } else if (this.state === 'adding data node') {
      this.placeNode(e.position, 'data');
      this.history.modify(Workflow.ACTION.ADD_NODE);
    }
  }

  placeNode(position, type) {
    let node;
    if (type === 'operation') {
      node = this.cy.add({
        group: "nodes",
        classes: 'operation',
        data: {
          name: 'Operation',
          type: "operation",
          id: this.counter.inc(),
          required: false,
          requiredText: '',
          color: "lightgrey",
          description: '',
          custom: '',
          operationTool:''
        },
        position
      });
    } else if (type === 'data') {
      node = this.cy.add({
        group: "nodes",
        classes: 'data',
        data: {
          name: 'Data',
          type: "data",
          id: this.counter.inc(),
          required: false,
          requiredText: '',
          color: "lightgrey",
          description: '',
          custom: '',
          dataFormat:''
        },
        position
      });
    }
    this.setState();
    this.cy.$(':selected').unselect();
    this.cy.$(`#${this.counter.get()}`).select();

    node.on('free', () => {
      this.history.modify(Workflow.ACTION.POSITION_CHANGE);
    });
    node.on('unselect', () => {
      if (this.dataHasChanged) {
        this.dataHasChanged = false;
        this.history.modify(Workflow.ACTION.DATA_CHANGE);
      }
    });

    return this.counter.get();
  }

  renderWrapNodes() {
    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');
    this.sidebar.render(sidebarWrap, {
      title: this.title,
      classes: classnames('main'),
      template: {
        multiple_selection: true,
      },
      mode: {
        edit: this.mode === Workflow.MODE.EDIT,
        view: this.mode === Workflow.MODE.VIEW,
      }
    });

    if (this.mode === Workflow.MODE.EDIT) {

      sidebarWrap.querySelector('.remove-nodes').addEventListener('click', () => {
        this.cy.$(':selected').remove();
        this.renderAddNode();
      });

      sidebarWrap.querySelector('.wrap-nodes').addEventListener('click', () => {

        let nodes = [];
        nodes.push({
          group: "nodes",
          classes: 'wrap',
          data: {
            type: 'wrap',
            id: this.counter.inc(),
            color: "lightgrey",
            name: ''
          },
          position: {x: 0, y: 0}
        });

        let list = this.cy.$('node:selected').slice();
        let edges = list.connectedEdges();

        list.forEach((node) => {
          let data = node.json();
          if (data.data.type !== 'wrap') {
            data.data.parent = this.counter.get();
            nodes.push(data);
          }
        });

        edges.forEach((edge) => {
          let data = edge.json();
          nodes.push(data);
        });

        this.cy.$(':selected').remove();

        nodes = this.cy.add(nodes);

        nodes.on('free', () => {
          this.history.modify(Workflow.ACTION.POSITION_CHANGE);
        });
        nodes.on('unselect', () => {
          if (this.dataHasChanged) {
            this.dataHasChanged = false;
            this.history.modify(Workflow.ACTION.DATA_CHANGE);
          }
        });

        this.cy.$(':selected').unselect();
        this.cy.$('#' + this.counter.get()).select();

        this.history.modify(Workflow.ACTION.WRAP_NODES);
      });
    }
  }

  unwrap() {
    if (this.dataHasChanged) {
      this.dataHasChanged = false;
      this.history.modify(Workflow.ACTION.DATA_CHANGE);
    }

    let nodes = [];
    let list = this.cy.$(':selected > *').slice();

    let edges = list.connectedEdges();

    list.forEach((node) => {
      let data = node.json();
      if (data.data.type !== 'wrap') {
        data.data.parent = null;
        nodes.push(data);
      }
    });
    edges.forEach((edge) => {
      let data = edge.json();
      nodes.push(data);
    });

    this.cy.$(':selected').remove();
    nodes = this.cy.add(nodes);

    nodes.on('free', () => {
      this.history.modify(Workflow.ACTION.POSITION_CHANGE);
    });
    nodes.on('unselect', () => {
      if (this.dataHasChanged) {
        this.dataHasChanged = false;
        this.history.modify(Workflow.ACTION.DATA_CHANGE);
      }
    });


    this.cy.$(':selected').unselect();

    this.renderAddNode();

    this.history.modify(Workflow.ACTION.UNWRAP_NODES);

  }

  populate(e) {
    if (this.cy.$(':selected').isEdge()) {
      this.renderEdgeParams();
      return;
    } else {
      if (this.cy.$(':selected').length === 0) {
        this.renderAddNode();
        return;
      }

      if (this.cy.$(':selected').length > 1) {
        this.renderWrapNodes();
        return;
      }

      if (e.target.data().type === 'wrap') {
        this.renderWrapParams(e.target.id(), e.target.data());
        this.setState();
      } else if (e.target.data().type === 'operation') {
        this.renderFillNode(e.target.id(), e.target.data(), 'operation');
        this.setState();
      } else if (e.target.data().type === 'data') {
        this.renderFillNode(e.target.id(), e.target.data(), 'data');
        this.setState();
      }
    }
  }

  renderEdgeParams() {
    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');
    this.sidebar.render(sidebarWrap, {
      title: this.title,
      classes: classnames('main'),
      template: {
        edge: true,
      },
      mode: {
        edit: this.mode === Workflow.MODE.EDIT,
        view: this.mode === Workflow.MODE.VIEW,
      }
    });

    if (this.mode === Workflow.MODE.EDIT) {
      sidebarWrap.querySelector('.remove-edge').addEventListener('click', () => {
        this.cy.$(':selected').remove();
        this.renderAddNode();

        this.history.modify(Workflow.ACTION.REMOVE_EDGE);
      });
    }
  }

  renderWrapParams() {
    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');
    this.sidebar.render(sidebarWrap, {
      title: this.title,
      classes: classnames('main'),
      template: {
        wrapper: true,
      },
      label: this.cy.$(':selected').data().name || "",
      mode: {
        edit: this.mode === Workflow.MODE.EDIT,
        view: this.mode === Workflow.MODE.VIEW,
      }
    });

    if (this.mode === Workflow.MODE.EDIT) {
      sidebarWrap.querySelector('.wrap-label').addEventListener('input', () => {
        this.dataHasChanged = true;

        this.cy.$(':selected').data().name = sidebarWrap.querySelector('.wrap-label').value;

        this.cy.$(':selected').removeClass('wrap').addClass('wrap');
      });

      sidebarWrap.querySelector('.unwrap').addEventListener('click', () => {
        this.unwrap();
      });
    }

  }

  renderAddNode() {
    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');
    this.sidebar.render(sidebarWrap, {
      title: this.title,
      classes: classnames('main'),
      template: {
        init: true,
      },
      first: this.empty,
      mode: {
        edit: this.mode === Workflow.MODE.EDIT,
        view: this.mode === Workflow.MODE.VIEW,
      }
    });

    if (this.mode === Workflow.MODE.EDIT) {
      sidebarWrap.querySelector('.add-operation-node').addEventListener('click', () => {
        if (this.empty) {
          this.addOperationNode();
          this.empty = false;
        } else {
          this.renderSelectLocation();
          this.setState('adding operation node');
        }
      });
      sidebarWrap.querySelector('.add-data-node').addEventListener('click', () => {
        if (this.empty) {
          this.addDataNode();
          this.empty = false;
        } else {
          this.renderSelectLocation();
          this.setState('adding data node');
        }
      });
    }
  }

  addOperationNode() {
    this.placeNode({
      x: this.cy.width() / 2,
      y: this.cy.height() / 4
    }, 'operation');
    this.history.modify(Workflow.ACTION.ADD_NODE);
  }

  addDataNode() {
    this.placeNode({
      x: this.cy.width() / 2,
      y: this.cy.height() / 4
    }, 'data');
    this.history.modify(Workflow.ACTION.ADD_NODE);
  }

  renderSelectLocation() {
    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');
    this.sidebar.render(sidebarWrap, {
      title: this.title,
      classes: classnames('main'),
      template: {
        select_location: true,
      },
      mode: {
        edit: this.mode === Workflow.MODE.EDIT,
        view: this.mode === Workflow.MODE.VIEW,
      }
    });

    if (this.mode === Workflow.MODE.EDIT) {
      sidebarWrap.querySelector('.cancel').addEventListener('click', () => {
        this.setState();
        if (this.cy.$(':selected').isNode()) {
          this.populate({target: this.cy.$(':selected')});
        } else {
          this.renderAddNode();
        }
      });
    }
  }

  renderAssocResources(nodeId, data, type) {
    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');

    this.sidebar.render(sidebarWrap, {
      title: 'Associated resources',
      template: {
        assoc_resources: true,
      },
      type: {
        operation_node: type == 'operation',
        data_node: type == 'data'
      },
      active: {
        assoc: true
      },
      mode: {
        edit: this.mode === Workflow.MODE.EDIT,
        view: this.mode === Workflow.MODE.VIEW,
      }
    });

    if (this.mode === Workflow.MODE.EDIT) {

      // Buttons

      sidebarWrap.querySelector(".add-next-node").addEventListener('click', () => {
        this.renderSelectLocation();
        this.setState('adding node');
      });
      sidebarWrap.querySelector('.add-edam-annotations').addEventListener('click', () => {
        this.renderFillNode(nodeId, data, type);
      });
      // Color header

      if (data.color) {
        sidebarWrap.querySelector('.workflow-diagram-sidebar-title').style.backgroundColor = data.color;
      }

      if (type=='data'){
        this._renderDatabaseSelector(sidebarWrap, data);
      } else if (type=='operation') {
        this._renderToolSelector(sidebarWrap, data);
      }
      this._renderTrainingSelector(sidebarWrap, data);

      // Description

      let renderPreview = () => {
        sidebarWrap.querySelector(".description-preview").innerHTML = this.md.render(data.description);
      };
      sidebarWrap.querySelector(".description").value = data.description;
      sidebarWrap.querySelector(".description").addEventListener('input', () => {
        this.dataHasChanged = true;
        data.description = sidebarWrap.querySelector(".description").value;
        renderPreview();
      });

      if (data.description) {
        renderPreview();
      }
      
      // Action button group
      sidebarWrap.querySelector(".remove-node").addEventListener('click', () => {
        this.removeNode(nodeId, type);
      });

      sidebarWrap.querySelector(".select-branch").addEventListener('click', () => {
        this.selectBranch(nodeId, type);
      });

      sidebarWrap.querySelector(".add-next-node").addEventListener('click', () => {
        this.addNextNode(nodeId, type, data.required);
      });

    }
  }

  _renderToolSelector(sidebarWrap, data) {
    // Tools selector

    let select = new Choices(sidebarWrap.querySelector(".choices"), {
      position: 'bottom',
      itemSelectText: '',
      placeholder: true,
      placeholderValue: 'Select associated tools...',
      searchResultLimit: 10,
      searchChoices: false,
      editItems: true,
      addItems: true,
      removeItemButton: true,
      shouldSort: false,
      classNames: {
        containerOuter: 'custom_choices',
        containerInner: 'custom_choices__inner',
        input: 'custom_choices__input',
        inputCloned: 'choices__input--cloned',
        list: 'custom_choices__list',
        listItems: 'custom_choices__list--multiple',
        listSingle: 'choices__list--single',
        listDropdown: 'custom_choices__list--dropdown',
        item: 'custom_choices__item',
        itemSelectable: 'custom_choices__item--selectable',
        itemDisabled: 'choices__item--disabled',
        itemOption: 'choices__item--choice',
        group: 'choices__group',
        groupHeading: 'choices__heading',
        button: 'custom_choices__button',
        activeState: 'is-active',
        focusState: 'custom_is-focused',
        openState: 'is-open',
        disabledState: 'is-disabled',
        highlightedState: 'custom_is-highlighted',
        hiddenState: 'is-hidden',
        flippedState: 'is-flipped',
        selectedState: 'is-highlighted',
      }
    });

    let customInput = {};
    let searchResults = {};
    let suggestions = {};
    let debounce;

    if (data.tools && data.tools.length > 0) {
      select.setValue(data.tools);
    }

    let fillSelect = () => {
      select.setChoices([customInput, searchResults, suggestions], 'value', 'label', true);
    };

    let clickable = document.createElement('div');
    clickable.className = 'custom_clickable';
    select.passedElement.parentNode.parentNode.appendChild(clickable);

    clickable.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();

      select.toggleDropdown();

      return false;
    });


    let getToolData = (id) => {
      var parser = document.createElement('a');
      parser.href = id;

      if (parser.origin === 'https://bio.tools'){
        var url = 'https://bio.tools/api/tool' + parser.pathname + '?format=json'
      } 
      if (url){
        return fetch(url)
        .then((resp) => resp.json())
        .then((json) => {
          var response_data = {
            description: json.description,
            homepage: json.homepage,
            version: json.version,
            link: json.link,
          }

          json.toolType.forEach((item) => {
            var rObj = {}
            rObj[item.toLowerCase().split(' ').join('_')] = true
            Object.assign(response_data, rObj) //this concats the types to the response hash
          })
          return response_data
        })
      } else {
        return null
      }
    }

    /* When the user selects a tool
        Go through the list of currently selected options
        If it does not have a URL (e.g. it is a custom input)
          See if there is a user inputted URL in data.tools
        Else if it a bio.tools URL
          Load extra metadata from bio.tools
     */
    select.passedElement.addEventListener('change', (e) => {
      this.dataHasChanged = true;
      data.tools = select.getValue().map((item) => {
          var tool_info = {
            value: item.value,
            label: item.label
          }
          //if its a custom input, load any user inputted urls
          if (tool_info['value'] === 'https://'){
              if (data && data.tools && data.tools !== undefined
                       && data.tools.constructor === Array){
                for(var i = 0; i < data.tools.length; i++) {
                  if (data.tools[i].label == tool_info['label']) {
                    tool_info['value'] = data.tools[i].value
                    break;
                  }
                }
              }
          }
          //if its a bio.tools entry, load extra data from bio.tools
          if (tool_info['value'].startsWith('https://bio.tools')){
            const tool_resp = getToolData(tool_info['value'])
            if (tool_resp) {
              tool_resp.then((response) => {if (response){tool_info['data'] = response}})
            }
          }
          return tool_info
        // var updatedNode = this.cy.$(':selected');
        // updatedNode.data().operationTool += item.label;
        // this.cy.remove(this.cy.$(':selected'));
        // this.cy.add(updatedNode);
        // this.cy.$(':selected').removeClass('operation').addClass('operation'); // force redraw
      })
      renderTools(e);
      return data.tools
    });

    
    let renderTools = (e) => {
      var previousNode = this.cy.$(':selected');
      var edges = previousNode.connectedEdges();
      previousNode.data().operationTool = '';
      this.cy.remove(this.cy.$(':selected'));
      this.cy.add(previousNode);
      for (var i = 0; i < edges.length; i++) {
        this.cy.add(edges[i]);
      }
      this.cy.$(':selected').removeClass('operation').addClass('operation');

      sidebarWrap.querySelector('.added-tools').innerHTML = "";
      data.tools && data.tools.forEach((tool, i) => {
        let toolData = {
          label: tool.label,
          value: tool.value,
          index: i,
          description: tool.description ? tool.description : "",
        };
        let toolsNode = document.createElement("li");
        toolsNode.innerHTML = templates.edit._tool_description(toolData);
        sidebarWrap.querySelector('.added-tools').appendChild(toolsNode);

        toolsNode.querySelectorAll('input')[0].addEventListener('input', (e) => {
          this.dataHasChanged = true;
          data.tools[i].label = e.target.value;
        });

        toolsNode.querySelectorAll('input')[1].addEventListener('input', (e) => {
          this.dataHasChanged = true;
          data.tools[toolData['index']].value = e.target.value;
        });

        toolsNode.querySelector('textarea').addEventListener('input', (e) => {
          this.dataHasChanged = true;
          data.tools[i].description = e.target.value;
        });        
        var updatedNode = this.cy.$(':selected');
        var edges = updatedNode.connectedEdges();
        updatedNode.data().operationTool += '<li>' + tool.label + '</li>';
        this.cy.remove(this.cy.$(':selected'));
        this.cy.add(updatedNode);
        for (var i = 0; i < edges.length; i++) {
          this.cy.add(edges[i]);
        }
        this.cy.$(':selected').removeClass('operation').addClass('operation');
      });
    };

    renderTools();

    select.passedElement.addEventListener('search', (e) => {
      clearTimeout(debounce);
      customInput = {
        label: 'Custom input',
        id: 1,
        selected: true,
        disabled: false,
        choices: [{value: "https://", label: e.detail.value}]
      };
      fillSelect();

      debounce = setTimeout(() => {
        let url = BIOTOOLS_API + encodeQueryString({format: 'json', q: e.detail.value, sort: 'score'});
        fetch(url)
          .then((resp) => resp.json())
          .then((json) => {
            let choices = json.list.map((tool) => {
              return {
                value: "https://bio.tools/" + tool.biotoolsID, // NOTE: tool.biotoolsID if BASE_URL is dev.bio.tools
                label: tool.name,
                data: tool
              }
            });

            searchResults = {
              label: 'Search results from bio.tools',
              id: 2,
              disabled: false,
              choices
            };
            fillSelect();
          }).catch(() => {
          sidebarWrap.querySelector('.biotools-status').style.display = 'block';
          console.log("error");
        });

      }, 500);
    })


    let operation = EdamSelect.getTermById('operation', data.operation);

    if (operation) {
      operation = operation[2];

      let params = {
        operation,
        sort: 'score',
        format: 'json'
      };

      // TODO: improve suggestions based on input data
      let url = BIOTOOLS_API + encodeQueryString(params);

      fetch(url)
        .then((resp) => resp.json())
        .then(function (json) {
          let choices = json.list.map((tool) => {
            // Value is constructed by the url + tool.name.
            return {
              value: "https://bio.tools/" + tool.biotoolsID, // NOTE: tool.biotoolsID if BASE_URL is dev.bio.tools
              label: tool.name,
              data: tool
            }
          });

          suggestions = {
            label: 'Suggestions based on EDAM annotations from bio.tools',
            id: 3,
            disabled: false,
            choices
          };
          fillSelect();

        }).catch((e) => {
        sidebarWrap.querySelector('.biotools-status').style.display = 'block';
        console.log("error");
      });
    }
  }
  

  _renderDatabaseSelector(sidebarWrap, data) {
    // Tools selector

    let select = new Choices(sidebarWrap.querySelector(".fairsharing-choices"), {
      position: 'bottom',
      itemSelectText: '',
      placeholder: true,
      placeholderValue: 'Select associated databases...',
      searchResultLimit: 10,
      searchChoices: false,
      editItems: true,
      addItems: true,
      removeItemButton: true,
      shouldSort: false,
      classNames: {
        containerOuter: 'custom_choices',
        containerInner: 'custom_choices__inner',
        input: 'custom_choices__input',
        inputCloned: 'choices__input--cloned',
        list: 'custom_choices__list',
        listItems: 'custom_choices__list--multiple',
        listSingle: 'choices__list--single',
        listDropdown: 'custom_choices__list--dropdown',
        item: 'custom_choices__item',
        itemSelectable: 'custom_choices__item--selectable',
        itemDisabled: 'choices__item--disabled',
        itemOption: 'choices__item--choice',
        group: 'choices__group',
        groupHeading: 'choices__heading',
        button: 'custom_choices__button',
        activeState: 'is-active',
        focusState: 'custom_is-focused',
        openState: 'is-open',
        disabledState: 'is-disabled',
        highlightedState: 'custom_is-highlighted',
        hiddenState: 'is-hidden',
        flippedState: 'is-flipped',
        selectedState: 'is-highlighted',
      }
    });

    let customInput = {};
    let searchResults = {};
    let suggestions = {};
    let debounce;

    if (data.databases && data.databases.length > 0) {
      select.setValue(data.databases);
    }

    let fillSelect = () => {
      select.setChoices([customInput, searchResults, suggestions], 'value', 'label', true);
    };

    let clickable = document.createElement('div');
    clickable.className = 'custom_clickable';
    select.passedElement.parentNode.parentNode.appendChild(clickable);

    clickable.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();

      select.toggleDropdown();

      return false;
    });
/*
    let getDatabaseData = (id) => {

        fetch(url, { 
          "headers": {
            "Api-Key": FAIRSHARING_API_KEY,
            'Content-Type': 'application/json; charset=utf-8'
          }
        })
      return fetch('')
        .then((resp) => resp.json())
        .then((json) => {
          var response_data = {
          }
        return {"description": "This is a description"} 
      })
    }

*/


    select.passedElement.addEventListener('change', (e) => {
      this.dataHasChanged = true;
      data.databases = select.getValue().map((item) => {
        var database_data = {
          value: item.value,
          label: item.label
        }
        /*getDatabaseData(item.value).then((response) => {database_data['data'] = response})*/
        return database_data
      });

    });

    select.passedElement.addEventListener('search', (e) => {
      clearTimeout(debounce);

      customInput = {
        label: 'Custom input',
        id: 1,
        selected: true,
        disabled: false,
        choices: [{value: e.detail.value, label: e.detail.value}]
      };
      fillSelect();

      debounce = setTimeout(() => {
        let url = FAIRSHARING_API + encodeQueryString({search: e.detail.value, type: 'biodbcore'});

        fetch(url, {
          "headers": {
            "Api-Key": FAIRSHARING_API_KEY,
            'Content-Type': 'application/json; charset=utf-8'
          }
        })
          .then((resp) => resp.json())
          .then((json) => {
            let choices = json.results.map((database) => {
              return {
                value: database.bsg_id + "", 
                label: database.name
              }
            });

            searchResults = {
              label: 'Search results from FAIRSharing.org',
              id: 2,
              disabled: false,
              choices
            };
            fillSelect();
          }).catch(() => {
          sidebarWrap.querySelector('.fairsharing-status').style.display = 'block';
          console.log("error");
        });

      }, 500);
    });

  }

  _renderTrainingSelector(sidebarWrap, data) {
    // Tools selector
    let select = new Choices(sidebarWrap.querySelector(".tess-choices"), {
      position: 'bottom',
      itemSelectText: '',
      placeholder: true,
      placeholderValue: 'Select useful training resour...',
      searchResultLimit: 10,
      searchChoices: false,
      editItems: true,
      addItems: true,
      removeItemButton: true,
      shouldSort: false,
      classNames: {
        containerOuter: 'custom_choices',
        containerInner: 'custom_choices__inner',
        input: 'custom_choices__input',
        inputCloned: 'choices__input--cloned',
        list: 'custom_choices__list',
        listItems: 'custom_choices__list--multiple',
        listSingle: 'choices__list--single',
        listDropdown: 'custom_choices__list--dropdown',
        item: 'custom_choices__item',
        itemSelectable: 'custom_choices__item--selectable',
        itemDisabled: 'choices__item--disabled',
        itemOption: 'choices__item--choice',
        group: 'choices__group',
        groupHeading: 'choices__heading',
        button: 'custom_choices__button',
        activeState: 'is-active',
        focusState: 'custom_is-focused',
        openState: 'is-open',
        disabledState: 'is-disabled',
        highlightedState: 'custom_is-highlighted',
        hiddenState: 'is-hidden',
        flippedState: 'is-flipped',
        selectedState: 'is-highlighted',
      }
    });

    let customInput = {};
    let searchResults = {};
    let suggestions = {};
    let debounce;

    if (data.trainings && data.trainings.length > 0) {
      select.setValue(data.trainings);
    }

    let fillSelect = () => {
      select.setChoices([customInput, searchResults, suggestions], 'value', 'label', true);
    };

    let clickable = document.createElement('div');
    clickable.className = 'custom_clickable';
    select.passedElement.parentNode.parentNode.appendChild(clickable);

    clickable.addEventListener('click', (e) => {
      e.preventDefault();
      e.stopPropagation();

      select.toggleDropdown();

      return false;
    });

      let contentProviderLogo = (id) => {
        return fetch('https://tess.elixir-europe.org/content_providers/' + id + '.json')
        .then((resp) => resp.json())
        .then((json) => {
          return json.title
        })
      }



    let getTrainingData = (id) => {
      return fetch('https://tess.elixir-europe.org/materials/' + id + '.json')
      .then((resp) => resp.json())
      .then((json) => {
        var response_data = {
          description: json.short_description
        }
        contentProviderLogo(json.content_provider_id)
        .then((response) => response_data['content_provider'] = response)
        return response_data
      })
    }


    select.passedElement.addEventListener('change', (e) => {
      this.dataHasChanged = true;
      data.trainings = select.getValue().map((item) => {
          var training_info = {
            value: item.value,
            label: item.label
          }
          getTrainingData(item.value).then((response) => training_info['data'] = response)
          return training_info
      })
    });

    select.passedElement.addEventListener('search', (e) => {
      clearTimeout(debounce);

      customInput = {
        label: 'Custom input',
        id: 1,
        selected: true,
        disabled: false,
        choices: [{value: e.detail.value, label: e.detail.value}]
      };
      fillSelect();

      debounce = setTimeout(() => {
        let url = TESS_API + encodeQueryString({q: e.detail.value});
        fetch(url)
          .then((resp) => resp.json())
          .then((json) => {
            let choices = json.map((training) => {
              return {
                value: training.id + "",
                label: training.title
              }
            });

            searchResults = {
              label: 'Search results from TeSS',
              id: 2,
              disabled: false,
              choices
            };
            fillSelect();
          })
          // .catch(() => {
          // sidebarWrap.querySelector('.tess-status').style.display = 'block';
          // console.log("error");
        // });

      }, 500);
    });


    let operation = EdamSelect.getTermById('operation', data.operation);

    if (operation) {
      operation = operation[2];

      let params = {
        operations: operation,
      };

      // TODO: improve suggestions based on input data

      let url = TESS_API + encodeQueryString(params);

      fetch(url)
        .then((resp) => resp.json())
        .then(function (json) {

          let choices = json.map((training) => {
            return {
              value: training.id + "",
              label: training.title
            }
          });

          suggestions = {
            label: 'Suggestions based on TeSS results from https://tess.elixir-europe.org',
            id: 3,
            disabled: false,
            choices
          };
          fillSelect();

        })
        // .catch(() => {
        // sidebarWrap.querySelector('.tess-status').style.display = 'block';
        // console.log("error");
      // });
    }
  }

  renderFillNode(nodeId, data, type) {

    let sidebarWrap = this.el.querySelector('.workflow-diagram-sidebar');
    let load_operations = (ops) => {
      if (ops){
        return ops.map((op)=>EdamSelect.getTermById('operation',op))
      } else { return null }
    }

    if (type === 'operation') {

      this.sidebar.render(sidebarWrap, {
        title: 'Operation node',
        template: {
          operation: true,
        },
        active: {
          edam: true
        },
        mode: {
          edit: this.mode === Workflow.MODE.EDIT,
          view: this.mode === Workflow.MODE.VIEW,
        },
        data: {
          markdown_description: this.md.render(data.description),
          ...data
        },
        terms: load_operations(data.operation),
        tools: data.tools,
        trainings: data.trainings
      });

      // console.log(data);
      // console.log(EdamSelect.getTermById('operation', data.operation))

      if (data.color) {
        sidebarWrap.querySelector('.workflow-diagram-sidebar-title').style.backgroundColor = data.color;
        if (this.mode === Workflow.MODE.VIEW){
          sidebarWrap.querySelector('.workflow-diagram-sidebar-content').style.backgroundColor = data.color;        
        }
      }

      if (this.mode === Workflow.MODE.EDIT) {

        let operationSelectNode = sidebarWrap.querySelector(".edam-operation");

        let params = {
          name: `operation:${nodeId}`,
          initDepth: 1,
          type: 'operation',
          inline: false,
          maxHeight: 300,
          multiselect: true,
          // opened: true,
        };

        if (data.operation) {
          params.preselected = data.operation;
        }

        let operationSelect = new EdamSelect(operationSelectNode, params);

        // tabs
        sidebarWrap.querySelector(".add-assoc-resources").addEventListener('click', () => {
          this.renderAssocResources(nodeId, data, type);
        });


        // custom?

        sidebarWrap.querySelector(".custom").addEventListener('click', () => {
          if (sidebarWrap.querySelector(".custom").checked) {
            sidebarWrap.querySelector(".custom-input").style.display = 'block';
            sidebarWrap.querySelector(".custom-input").focus();
            sidebarWrap.querySelector(".custom-input").value = data.custom;

            sidebarWrap.querySelector(".edam-operation").style.display = 'none';
          } else {
            sidebarWrap.querySelector(".custom-input").style.display = 'none';
            sidebarWrap.querySelector(".custom-input").value = '';

            sidebarWrap.querySelector(".edam-operation").style.display = 'block';
          }
        });

        sidebarWrap.querySelector(".custom-input").addEventListener('input', (e) => {
            data.custom = sidebarWrap.querySelector(".custom-input").value;
            this._edamControl();
          // data.custom = sidebarWrap.querySelector(".custom-input").value;
          // document.querySelector('pre').innerText = this.value;
        });

        if (data.custom) {
          sidebarWrap.querySelector(".custom").checked = 'checked';
          sidebarWrap.querySelector(".custom-input").style.display = 'block';
          sidebarWrap.querySelector(".custom-input").value = data.custom;

          sidebarWrap.querySelector(".edam-operation").style.display = 'none';
        } else {
          sidebarWrap.querySelector(".required").checked = false;
          sidebarWrap.querySelector(".custom-input").style.display = 'none';
          sidebarWrap.querySelector(".custom-input").value = '';

          sidebarWrap.querySelector(".edam-operation").style.display = 'block';
        }

      }
    } else if (type === 'data') {
      this.sidebar.render(sidebarWrap, {
        title: 'Data node',
        template: {
          data: true,
        },
        active: {
          edam: true
        },
        mode: {
          edit: this.mode === Workflow.MODE.EDIT,
          view: this.mode === Workflow.MODE.VIEW,
        },
        data: {
          names: data.name.split('\n'), 
          markdown_description: this.md.render(data.description),
          ...data
        },
        terms: [EdamSelect.getTermById('data', data.data)],
        trainings: data.trainings,
        databases: data.databases
      });

      if (this.mode === Workflow.MODE.EDIT) {

        let dataSelectNode = sidebarWrap.querySelector(".edam-data");

        let params = {
          name: `data:${nodeId}`,
          initDepth: 1,
          type: 'data',
          inline: false,
          maxHeight: 300,
          multiselect: false,
          // opened: true,
        };

        if (data.data) {
          params.preselected = [data.data];
        }

        let dataSelect = new EdamSelect(dataSelectNode, params);


        // formats

        params = {
          name: `formats:${nodeId}`,
          type: 'format',
          inline: false,
          maxHeight: 300,
          multiselect: true
        };

        if (data.formats) {
          params.preselected = data.formats;
        }

        new EdamSelect(sidebarWrap.querySelector('.edam-formats'), params);


        // custom?

        sidebarWrap.querySelector(".custom").addEventListener('click', () => {
          if (sidebarWrap.querySelector(".custom").checked) {
            sidebarWrap.querySelector(".custom-input").style.display = 'block';
            sidebarWrap.querySelector(".custom-input").focus();
            sidebarWrap.querySelector(".custom-input").value = data.custom;

            sidebarWrap.querySelector(".edam-data").style.display = 'none';
          } else {
            sidebarWrap.querySelector(".custom-input").style.display = 'none';
            sidebarWrap.querySelector(".custom-input").value = '';

            sidebarWrap.querySelector(".edam-data").style.display = 'block';
          }
        });

        sidebarWrap.querySelector(".custom-input").addEventListener('input', (e) => {
          data.custom = sidebarWrap.querySelector(".custom-input").value;
          this._edamControl();
          // data.custom = sidebarWrap.querySelector(".custom-input").value;
          // document.querySelector('pre').innerText = this.value;
        });

        if (data.custom) {
          sidebarWrap.querySelector(".custom").checked = 'checked';
          sidebarWrap.querySelector(".custom-input").style.display = 'block';
          sidebarWrap.querySelector(".custom-input").value = data.custom;

          sidebarWrap.querySelector(".edam-data").style.display = 'none';
        } else {
          sidebarWrap.querySelector(".required").checked = false;
          sidebarWrap.querySelector(".custom-input").style.display = 'none';
          sidebarWrap.querySelector(".custom-input").value = '';

          sidebarWrap.querySelector(".edam-data").style.display = 'block';
        }

                // tabs
        sidebarWrap.querySelector(".add-assoc-resources").addEventListener('click', () => {
          this.renderAssocResources(nodeId, data, type);
        });

      }
    }

    if (this.mode === Workflow.MODE.EDIT) {
      // Required
      if (data.required) {
        sidebarWrap.querySelector(".required").checked = 'checked';
        sidebarWrap.querySelector(".required-description").style.display = 'block';
        // this.renderEdges(nodeId, data.required)
      } else {
        sidebarWrap.querySelector(".required").checked = false;
        sidebarWrap.querySelector(".required-description").style.display = 'none';
      }

      sidebarWrap.querySelector(".required").addEventListener('change', () => {
        this.dataHasChanged = true;
        if (sidebarWrap.querySelector(".required").checked) {
          sidebarWrap.querySelector(".required-description").style.display = 'block';
          data.required = true;
          this.renderEdges(nodeId, data.required)
        } else {
          sidebarWrap.querySelector(".required-description").style.display = 'none';
          data.required = false;
          this.renderEdges(nodeId, data.required)
        }
      });

      sidebarWrap.querySelector(".required-description").value = data.requiredText;
      sidebarWrap.querySelector(".required-description").addEventListener('input', () => {
        this.dataHasChanged = true;
        data.requiredText = sidebarWrap.querySelector(".required-description").value;
      });

      // Button group
      sidebarWrap.querySelector(".remove-node").addEventListener('click', () => {
        this.removeNode(nodeId, type);
      });

      sidebarWrap.querySelector(".select-branch").addEventListener('click', () => {
        this.selectBranch(nodeId, type);
      });

      sidebarWrap.querySelector(".add-next-node").addEventListener('click', () => {
        this.addNextNode(nodeId, type, data.required);
      });
    }

  }

  removeNode(nodeId, type) {
    if (this.dataHasChanged) {
      this.dataHasChanged = false;
      this.history.modify(Workflow.ACTION.DATA_CHANGE);
    }

    this.cy.$("#" + nodeId).remove();
    this.history.modify(Workflow.ACTION.REMOVE_NODE);
    this.renderAddNode();
  }

  selectBranch(nodeId, type) {
    let successors = this.cy.$("#" + nodeId).successors();
    this.cy.$("#" + nodeId).select();
    successors.select();
  }

  addNextNode(nodeId, type, required) {
    let nodeType = type === 'operation' ? 'data' : 'operation';

    let successors = this.cy.$("#" + nodeId).successors();
    let outgoers = this.cy.$("#" + nodeId).outgoers('node');
    successors.shift('x', -100);

    let newNodeId = this.placeNode({
      x: this.cy.$("#" + nodeId).position().x + 100 * outgoers.length,
      y: this.cy.$("#" + nodeId).position().y + 100
    }, nodeType);

    this.cy.add({
      group: "edges",
      classes: required ? 'required' : null,
      data: {
        source: nodeId,
        target: newNodeId
      }
    });

    this.history.modify(Workflow.ACTION.ADD_NEXT_NODE);

    let pan = this.cy.extent().y2 - (this.cy.$("#" + newNodeId).position().y + this.cy.$("#" + newNodeId).height() / 2);

    if (pan < 0) {
      this.cy.animate({
        panBy: {y: pan - 25},
      }, {
        duration: 500
      });
    }
  }


  renderEdges(nodeId, required) {
    if (required) {
      this.cy.$(`#${nodeId}`).connectedEdges().addClass('required');
    } else {
      this.cy.$(`#${nodeId}`).connectedEdges().removeClass('required');
    }
  }

  initEdgeHandlers() {
    this.cy.edgehandles({
      handleNodes: '.operation, .data',
      handlePosition: (node) => {
        return 'middle bottom';
      },

      complete: (sourceNode, targetNodes, addedEntities) => {
        if (targetNodes.data().type === sourceNode.data().type) {
          addedEntities.remove();
        } else {
          this.history.modify(Workflow.ACTION.ADD_EDGE);
        }
      }
    });

    this.cy.on('ehpreviewon', (event, sourceNode, targetNode, addedEles) => {
      if (sourceNode.data().type === targetNode.data().type) {
        targetNode.addClass('wrong-connection');
      }
    });
    this.cy.on('ehpreviewoff ehcancel ehcomplete', (event, sourceNode, targetNode, addedEles) => {
      targetNode.removeClass('wrong-connection');
    });
  }

  export(elementsOnly = true, empty = true) {
    this.cy.elements('.eh-handle').remove();
    this.cy.$(':selected').unselect();

    if (elementsOnly) {
      return this.cy.json()['elements'];
    } else {
      return this.cy.json();
    }
  }

  import(data) {
    this.destroy();
    this.init(data);

    this.history.modify(Workflow.ACTION.INIT);
    this.renderAddNode(true);


    this.cy.layout({ name: 'dagre'}).run();
    this.history.modify(Workflow.ACTION.LAYOUT);
  }

  destroy() {
    document.removeEventListener('keydown', this.keyboardControlWrap);
    document.removeEventListener('edam:change', this.edamControl);
    document.removeEventListener('click', this.focusControl);
  }

  static incrementId() {
    if (!this.latestId) {
      this.latestId = 1;
    }
    else {
      this.latestId++;
    }
    return this.latestId;
  }
}

Workflow.MODE = {
  VIEW: 1,
  EDIT: 2
};

Workflow.ACTION = {
  ADD_NODE: 1,
  ADD_EDGE: 2,
  WRAP_NODES: 3,
  REMOVE_NODE: 4,
  REMOVE_EDGE: 5,
  UNWRAP_NODES: 6,
  DATA_CHANGE: 7,
  POSITION_CHANGE: 8,
  ADD_NEXT_NODE: 9,
  INIT: 10,
  LAYOUT: 11,
};


// export stuff...
let root = typeof self == 'object' && self.self === self && self ||
  typeof global == 'object' && global.global === global && global ||
  this ||
  {};

if (typeof exports != 'undefined' && !exports.nodeType) {
  if (typeof module != 'undefined' && !module.nodeType && module.exports) {
    exports = module.exports = _;
  }
  exports.Workflow = Workflow;
} else {
  root.Workflow = Workflow;
}

if (typeof define == 'function' && define.amd) {
  define('Workflow', [], function () {
    return Workflow;
  });
}


