const TMLP_PATH = "../templates";

import layout from "../templates/layout.hbs"
import sidebar from "../templates/sidebar.hbs"
import docs from "../templates/docs.hbs"

import view_data from "../templates/view_mode/data.hbs"
import view_operation from "../templates/view_mode/operation.hbs"
import view_wrapper from "../templates/view_mode/wrapper.hbs"
import view_workflow from "../templates/view_mode/workflow.hbs"

import edit_assoc_resources from "../templates/edit_mode/assoc_resources.hbs"
import edit_data from "../templates/edit_mode/data.hbs"
import edit_operation from "../templates/edit_mode/operation.hbs"
import edit_wrapper from "../templates/edit_mode/wrapper.hbs"
import edit_edge from "../templates/edit_mode/edge.hbs"
import edit_multiple_selection from "../templates/edit_mode/multiple_selection.hbs"
import edit_select_location from "../templates/edit_mode/select_location.hbs"
import edit_workflow from "../templates/edit_mode/workflow.hbs"

import edit__tool_description from "../templates/edit_mode/_tool_description.hbs"

import converter from "../templates/converter.hbs"

export default {
  layout: layout,
  sidebar: sidebar,
  docs: docs,
  converter: converter,

  view: {
    data: view_data,
    operation: view_operation,
    wrapper: view_wrapper,
    workflow: view_workflow,
  },
  edit: {
    assoc_resources: edit_assoc_resources,
    data: edit_data,
    operation: edit_operation,
    wrapper: edit_wrapper,
    edge: edit_edge,
    multiple_selection: edit_multiple_selection,
    select_location: edit_select_location,
    workflow: edit_workflow,
    _tool_description: edit__tool_description,
  },
}
